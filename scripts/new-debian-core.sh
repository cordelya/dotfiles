#!/bin/bash

# New Machine Setup Script for Debian-based systems

# Core Apps

sudo apt update
sudo apt upgrade -y
sudo apt install ansible -y
sudo apt install avahi-daemon -y
sudo apt install figlet -y
sudo apt install golang-go -y
sudo apt install lolcat -y
sudo apt install mailutils -y
sudo apt install micro -y
sudo apt install python3-pip -y
sudo apt install python3-venv -y
sudo apt install rsync -y
sudo apt install samba -y
sudo apt install smbclient -y
sudo apt install speedtest-cli -y

# Tailscale
curl -fsSL https://pkgs.tailscale.com/stable/ubuntu/jammy.noarmor.gpg | sudo tee /usr/share/keyrings/tailscale-archive-keyring.gpg >/dev/null
curl -fsSL https://pkgs.tailscale.com/stable/ubuntu/jammy.tailscale-keyring.list | sudo tee /etc/apt/sources.list.d/tailscale.list
sudo apt update
sudo apt install tailscale
tailscale up
