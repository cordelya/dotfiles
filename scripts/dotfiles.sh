#!/bin/bash

# Script to clone dotfiles repo to local machine and do basic setup

git clone https://gitlab.com/cordelya/dotfiles.git

rm .bashrc
rm .vimrc
rm .gitignore
rm .gitconfig
ln -s $HOME/dotfiles/configs/.bashrc $HOME/.bashrc
ln -s $HOME/dotfiles/configs/.bashrc.d $HOME/.bashrc.d
ln -s $HOME/dotfiles/configs/.vimrc $HOME/.vimrc
ln -s $HOME/dotfiles/configs/.vimrc.d $HOME/.vimrc.d
ln -s $HOME/dotfiles/configs/.gitconfig $HOME/.gitconfig
ln -s $HOME/dotfiles/configs/.gitignore $HOME/.gitignore
ln -s $HOME/dotfiles/configs/.gitconfig.local $HOME/.gitconfig.local
