#!/bin/bash

# Adapted from https://github.com/paulmillr/dotfiles/blob/master/git-extras/git-setup.sh
repo="$1"

if [ -z "$repo" ]; then
  echo "Syntax: git setup user/project"
  echo "Example: git setup cordelya/dotfiles"
  exit
fi

mkdir -p "$dev/$repo" && \
  cd "$dev/$repo" && \
  git init && \
  touch 'README.md' 'CHANGELOG.md' && \
  echo "#$repo" >> 'README.md' && \
  cp "$HOME/.gitignore" '.gitignore' && \
  git add "*" && git add '.gitignore' && \
  git commit -m 'Initial commit.' && \
  git remote add origin "git@gitlab.com:$repo" && \
  git push -u origin main
