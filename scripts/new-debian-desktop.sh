#!/bin/bash

# New System Apps for Desktop Systems

# Start with the Core apps
#./$HOME/dotfiles/scripts/new-debian-core.sh

# Add Desktop-specific apps

# Image Manipulation & Design
sudo apt install blender -y
sudo apt install inkscape -y
sudo apt install krita -y

# Document/File/Web Authoring Tools
sudo apt install hugo -y
sudo apt install pandoc -y
sudo apt install texstudio -y
sudo apt install openscad -y
sudo apt install treesheets -y

# Audio Tools
sudo apt install audacity -y
sudo apt install picard -y

# Comms
sudo apt install discord -y
sudo apt install obs-studio -y
sudo apt install thunderbird -y

# Utilities
sudo apt install lynx -y
sudo apt install chromium-browser -y
sudo apt install chirp -y
sudo apt install diceware -y
sudo apt install gopher -y
sudo apt install yubikey-manager-qt -y
sudo apt install openjdk-8-jre -y
sudo apt install openjdk-11-jre -y
sudo apt install yamllint -y
sudo apt install zsh -y

# Gaming
sudo apt install aisleriot -y
sudo apt install steam-installer -y


#Prism Launcher
curl -q 'https://proget.makedeb.org/debian-feeds/prebuilt-mpr.pub' | gpg --dearmor | sudo tee /usr/share/keyrings/prebuilt-mpr-archive-keyring.gpg 1> /dev/null
echo "deb [signed-by=/usr/share/keyrings/prebuilt-mpr-archive-keyring.gpg] https://proget.makedeb.org prebuilt-mpr $(lsb_release -cs)" | sudo tee /etc/apt/sources.list.d/prebuilt-mpr.list
sudo apt update
sudo apt install prismlauncher -y



# Slack Desktop

curl -s https://packagecloud.io/install/repositories/slacktechnologies/slack/script.deb.sh | sudo bash

## Charmbracelet Apps
#sudo mkdir -p /etc/apt/keyrings
#curl -fsSL https://repo.charm.sh/apt/gpg.key | sudo gpg --dearmor -o /etc/apt/keyrings/charm.gpg
#echo "deb [signed-by=/etc/apt/keyrings/charm.gpg] https://repo.charm.sh/apt/ * *" | sudo tee /etc/apt/sources.list.d/charm.list
#apt update
#apt install glow
#apt install wishlist

echo "All done! If you want Zoom or Snapmaker Luban, you will need to manually install them by obtaining their respective .deb installers"
