#!/bin/bash

# Message of the Day
# Adapted from https://github.com/Lissy93/dotfiles/blob/master/utils/welcome-banner.sh

# Variables
COLOR_P='\033[1;36m'
COLOR_S='\033[0;36m'
COLOR_B='\033[1;34m'
RESET='\033[0m'

# Fun greeting
function motd_greets () {
  h=`date +%H`
  if [ $h -lt 04 ]; then 
    greeting="Good night"
  elif [ $h -lt 12 ]; then 
    greeting="Good morning"
  elif [ $h -lt 18 ]; then 
    greeting="Good afternoon"
  elif [ $h -gt 18 ]; then 
    greeting="Good evening"
  else
    greeting="Hello"
  fi
  WELCOME_MSG="$greeting $USER!"
  if hash lolcat 2>/dev/null && hash figlet 2>/dev/null; then
    echo "${WELCOME_MSG}" | figlet | lolcat
  else
    echo -e "$COLOR_PS{WELCOME_MSG}${RESET}\n"
  fi
}

# Print info from neofetch
function motd_sysinfo () {
  if hash neofetch 2>/dev/null; then
    neofetch --shell_version off \
      --disable kernel distro shell resolution de wm wm_theme icons terminal \
      --backend off \
      --colors 4 8 4 4 8 6 \
      --color_blocks off \
      --memory_display info
  fi
}

# Print current info
function motd_today () {
  timeout=10
  today_text="Today\n-------"
  echo -e "$COLOR_B$today_text"
  echo -e "$COLOR_S$(date '+🗓️  Date: %A, %B %d, %Y at %H:%M')"
  
  echo -n "⛅ " && curl -s -m $timeout "https://wttr.in?format=Weather:+%C+and+%t+with+Wind+%w.+Sunrise+at+%S+and+Sunset+at+%s."
  echo -e "${RESET}"

  if hash ip 2>/dev/null; then
    ip_address=$(ip route get 8.8.8.8 | awk -F"src " 'NR==1{split($2,a," ");print a[1]}')
    ip_interface=$(ip route get 8.8.8.8 | awk -F"dev " 'NR==1{split($2,a," ");print a[1]}')
    echo -e "${COLOR_S}🌐 IP: $(curl -s -m $timeout 'https://ipinfo.io/ip') (${ip_address} on ${ip_interface})"
    echo -e "${RESET}\n"
  fi
  task next
}

# Putting it all together
function welcome() {
  motd_greets
  motd_sysinfo
  motd_today
}

# Determine if file is being run directly or sourced
([[ -n $ZSH_EVAL_CONTEXT && $ZSH_EVAL_CONTEXT =~ :file$ ]] ||
  [[ -n $KSH_VERSION && $(cd "$(dirname -- "$0")" &&
    printf '%s' "${PWD%/}/")$(basename -- "$0") != "${.sh.file}" ]] ||
  [[ -n $BASH_VERSION ]] && (return 0 2>/dev/null)) && sourced=1 || sourced=0

# If script being called directly run immediately
if [ $sourced -eq 0 ]; then
  welcome $@
fi

# EOF
