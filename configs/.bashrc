# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# this .bashrc uses split files, stored in ~/.bashrc.d. This will source the
# contents of those files in the regular sort order. When you want to add
# some setting to .bashrc, either add it to a relevant existing file, or
# create a new one and name it based on where in the source order you want
# it to happen

for file in ~/.bashrc.d/*.bashrc;
do
source $file
done
