" ~/.vimrc: executed by vim

" this .vimrc uses split files, stored in ~/.vimrc.d. 
"
" To build your .vimrc file, do
"
" $ cat $HOME/.vimrc.d/*.vimrc > $HOME/.vimrc
" 
" This will concatenate the contents of all .vimrc files in the regular sort 
" order. When you want to add some setting to .vimrc, either add it to a 
" relevant existing file, or create a new one and name it based on where in the
" source order you want it to appear in the final file. 
" Find optional add-ins in the extras folder. Copy them into the parent folder
" to have them included in the build, then re-run the cat command.
"
" =============================================================================
" File 00-first.vimrc: First Things First
" =============================================================================
" everything in this file is stuff that must be done before anything else
" and this file must appear first in the sort order.

" When started as "evim", evim.vim will already have done these settings.
if v:progname =~? "evim"
  finish
endif

" Bail out if something that ran earlier, e.g. a system wide vimrc, does not
" want Vim to use these default values.
if exists('skip_defaults_vim')
  finish
endif

" Use Vim settings, rather than Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
" Avoid side effects when it was already reset.
if &compatible
  set nocompatible
endif

" When the +eval feature is missing, the set command above will be skipped.
" Use a trick to reset compatible only when the +eval feature is missing.
silent! while 0
  set nocompatible
silent! endwhile
