" The head of the vundle config section. Append all vundle plugin calls to
" 01-01-vundle-plugins.vimrc. All files in this directory with 'vundle' in
" the name are required in order for vundle to work.

filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')
