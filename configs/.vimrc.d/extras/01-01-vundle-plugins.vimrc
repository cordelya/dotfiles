" you can safely append vundle plugin calls to the end of this file via cli
" this file only holds plugin names. Anything that should come after the vundle
" section in the .vimrc should be placed in the sort order after all files with 
" 'vundle' in the filename.

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" WakaTime plugin (wakatime.com)
Plugin 'wakatime/vim-wakatime'

" Wakatime
let g:wakatime_PythonBinary = '/usr/bin/python3' " (Default: 'python')  "
