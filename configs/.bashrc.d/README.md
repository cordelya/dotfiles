# Cordelya's Split .bashrc Files #

These are Cordelya's .bashrc files. To use these, replace everything in ~/.bashrc with:

```
for file in ~/.bashrc.d/*.bashrc;
do
source $file
done
```

If you git cloned the files to a location other than ~/.bashrc.d, make some links:
ln -s ~/path/to/repo/.bashrc.d


Make sure that all of the .bashrc files in this folder, as well as the folder itself, are executable. 
If you did a symlink, you need to perform the chmod actions on the actual locations (so change the path).

```
chmod 700 ~/.bashrc.d
chmod +x ~/.bashrc.d/*.bashrc
```

## Extras ##
Items in the `extras` directory are optional .bashrc files you can copy up one level to have included.
They stay in the `extras` directory because they aren't always needed or wanted.
