# aliases & functions for bash on debian-based systems

# apt
alias aptupd="sudo apt-get update && apt list --upgradable"
alias aptupg="sudo apt-get upgrade -y"
