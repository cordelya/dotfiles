# this will only run if you have git cloned the bash-git-prompt files:
# git clone https://github.com/magicmonty/bash-git-prompt.git ~/.bash-git-prompt --depth=1

if [ -f "$HOME/.bash-git-prompt/gitprompt.sh" ]; then
    GIT_PROMPT_ONLY_IN_REPO=1
    source $HOME/.bash-git-prompt/gitprompt.sh
else
echo "git bash prompt is not available. see $HOME/.bashrc.d/01-git-prompt.bashrc for help"
fi
