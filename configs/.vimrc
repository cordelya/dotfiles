" ~/.vimrc: executed by vim

" this .vimrc uses split files, stored in ~/.vimrc.d. 
"
" To build your .vimrc file, do
"
" cat $HOME/.vimrc.d/*.vimrc > $HOME/.vimrc
" 
" This will concatenate the contents of all .vimrc files in the regular sort 
" order. When you want to add some setting to .vimrc, either add it to a 
" relevant existing file, or create a new one and name it based on where in the
" source order you want it to appear in the final file. 
" Find optional add-ins in the extras folder.
